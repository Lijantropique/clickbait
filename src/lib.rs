mod headers;

use headers::Headers;
use rand::seq::SliceRandom;
use rand::Rng;
use std::io::{self, Write};

pub const OBJECT_PRONOUNS: [&str; 3] = ["Her", "Him", "Them"];
pub const POSSESIVE_PRONOUNS: [&str; 3] = ["Her", "His", "Their"];
pub const PERSONAL_PRONOUNS: [&str; 3] = ["She", "He", "They"];
pub const PROFESSIONS: [&str; 6] = [
    "Doctor",
    "Plumber",
    "Teacher",
    "Bank Teller",
    "Priest",
    "Engineer",
];
pub const NOUNS: [&str; 15] = [
    "Refrigerator",
    "Vacuum Cleaner",
    "Shovel",
    "Paleo Diet",
    "Wine Cooler",
    "Solar Panel",
    "Computer",
    "Stove",
    "Printer",
    "Robot",
    "Video Game",
    "Television",
    "Plastic Straw",
    "Keyboard",
    "Telephone",
];
pub const STATES: [&str; 10] = [
    "California",
    "Texas",
    "Florida",
    "New York",
    "Pennsylvania",
    "Illinois",
    "Ohio",
    "Georgia",
    "North Carolina",
    "Michigan",
];
pub const PLACES: [&str; 8] = [
    "House",
    "Attic",
    "Bank Deposit Box",
    "School",
    "Basement",
    "Workplace",
    "Donut Shop",
    "Apocalypse Bunker",
];
pub const WHEN: [&str; 5] = ["Soon", "This Year", "Later Today", "RIGHT NOW", "Next Week"];

pub fn banner(msg: &str) {
    println!(
        "----------------------
{msg}
By Al Sweigart al@inventwithpython.com
(rust-ed 🦀 by lij)
----------------------\n"
    );
}

pub fn get_input(msg: &str) -> String {
    println!("{msg}");
    print!("> ");
    let mut raw = String::new();
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut raw)
        .expect("Failed to read line");

    raw.trim().to_string()
}

pub fn get_number_adds() -> u32 {
    println!("Our website needs to trick people into looking at ads!");
    let mut adds_number: u32;
    loop {
        let raw = get_input("Enter the number of clickbait headlines to generate (1-20):");
        adds_number = raw.parse().unwrap_or(0);
        if adds_number > 0 && adds_number < 21 {
            return adds_number;
        } else {
            println!("Only positive numbers between 1 and 20 are allowed\n");
        }
    }
}

pub fn create_add_vector(adds_number: u32) -> Vec<Headers> {
    println!("\n[+] Generating {adds_number} ads ...\n");

    let mut adds: Vec<Headers> = Vec::new();
    for _ in 0..adds_number {
        let add = generate_add();
        adds.push(add);
    }
    adds
}

fn generate_add() -> Headers {
    let mut rng = rand::thread_rng();
    let n = rand::thread_rng().gen_range(1..8);

    match n {
        1 => Headers::Millenial(NOUNS.choose(&mut rng).unwrap().to_string()),
        2 => Headers::KillYou {
            noun1: NOUNS.choose(&mut rng).unwrap().to_string(),
            noun2: NOUNS.choose(&mut rng).unwrap().to_string(),
            when: WHEN.choose(&mut rng).unwrap().to_string(),
        },
        3 => Headers::Invention {
            state: STATES.choose(&mut rng).unwrap().to_string(),
            profession: PROFESSIONS.choose(&mut rng).unwrap().to_string(),
            noun: NOUNS.choose(&mut rng).unwrap().to_string(),
        },
        4 => Headers::Discovery {
            state: STATES.choose(&mut rng).unwrap().to_string(),
            profession: PROFESSIONS.choose(&mut rng).unwrap().to_string(),
            pronoun: POSSESIVE_PRONOUNS.choose(&mut rng).unwrap().to_string(),
            place: PLACES.choose(&mut rng).unwrap().to_string(),
        },
        5 => Headers::DontKnow {
            profession: PROFESSIONS.choose(&mut rng).unwrap().to_string(),
            noun: NOUNS.choose(&mut rng).unwrap().to_string(),
        },
        6 => Headers::Gift {
            n: rand::thread_rng().gen_range(10..20),
            profession: PROFESSIONS.choose(&mut rng).unwrap().to_string(),
            state: STATES.choose(&mut rng).unwrap().to_string(),
        },
        _ => Headers::Reasons {
            n: rand::thread_rng().gen_range(10..20),
            noun: NOUNS.choose(&mut rng).unwrap().to_string(),
            m: rand::thread_rng().gen_range(5..10),
        },
    }
}

pub fn print_adds(adds: Vec<Headers>) {
    for (idx, add) in adds.iter().enumerate() {
        println!("{} - {}", idx + 1, add);
    }
}
