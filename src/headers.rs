use std::fmt;
pub enum Headers {
    Millenial(String),
    KillYou {
        noun1: String,
        noun2: String,
        when: String,
    },
    Invention {
        state: String,
        profession: String,
        noun: String,
    },
    Discovery {
        state: String,
        profession: String,
        pronoun: String,
        place: String,
    },
    DontKnow {
        profession: String,
        noun: String,
    },
    Gift {
        n: i32,
        profession: String,
        state: String,
    },
    Reasons {
        n: i32,
        noun: String,
        m: i32,
    },
}

impl std::fmt::Display for Headers {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> fmt::Result {
        let header = match self {
            Self::Millenial(n) => format!("Are Millennials Killing the {} Industry?", n),
            Self::KillYou { noun1, noun2, when } => {
                format!("Without This {}, {}s Could Kill You {}", noun1, noun2, when)
            }
            Self::Invention {
                state,
                profession,
                noun,
            } => format!(
                "Big Companies Hate it! See How This {} {} Invented a Cheaper {}",
                state, profession, noun
            ),
            Self::Discovery {
                state,
                profession,
                pronoun,
                place,
            } => format!(
                "You Won\'t Believe What This {} {} Found in {} {}",
                state, profession, pronoun, place
            ),
            Self::DontKnow {
                profession,
                noun,
            } => format!(
                "What {}s Don\'t Want You To Know About {}s",
                profession, noun
            ),
            Self::Gift {
                n,
                profession,
                state,
            } => format!(
                "{} Gift Ideas to Give Your {} From {}",
                n, profession, state
            ),
            Self::Reasons {
                n,
                noun,
                m,
            } => format!(
                "{} Reasons Why {}s Are More Interesting Than You Think (Number {} Will Surprise You!)",
                n, noun, m
            ),
        };

        write!(f, "{}", header)
    }
}
