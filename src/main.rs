fn main() {
    clickbait::banner("🎣 Clickbait Headline Generator 🎣");
    let adds_number = clickbait::get_number_adds();
    let adds = clickbait::create_add_vector(adds_number);
    clickbait::print_adds(adds);
}
